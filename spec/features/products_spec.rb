require 'rails_helper'

RSpec.describe "商品詳細ページ", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, name: "test bag", price: "1.00", taxons: [taxon]) }
  let!(:other_product) { create(:product, name: "test rails", price: "200.00", taxons: [other_taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  describe "商品詳細" do
    before { visit potepan_product_path(product.id) }

    it "商品詳細ページからホームページに移動できること" do
      within(".breadcrumb") do
        click_on "Home"
        expect(current_path).to eq potepan_path
      end
      expect(page).to have_content "人気カテゴリー", "新着商品"
    end

    it "一覧へ戻るからカテゴリーページに移動できること" do
      within(".media-body") do
        click_on "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    context "関連商品がある場合" do
      it "関連商品をクリックすると、その商品の詳細ページに跳ぶこと" do
        first(".productBox") do
          click_on related_products.first.name
          expect(current_path).to eq potepan_product_path(related_products.first.id)
        end
      end
    end
  end
end
