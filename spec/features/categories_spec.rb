require 'rails_helper'

RSpec.feature "商品一覧ページ", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxons) { taxonomy.root.children.create(name: "Categories") }
  let(:taxon1) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: "Rails", taxonomy: taxonomy) }
  let!(:product1) { create(:product, name: "test bag", price: "1.00", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "test rails", price: "200.00", taxons: [taxon2]) }

  describe '商品の小分類' do
    before { visit potepan_category_path(taxon1.id) }

    it '商品の小分類に関連した情報が取得できていること' do
      expect(page).to have_content "Bags"
      expect(page).to have_content "test bag"
      expect(page).to have_content "1.00"
      expect(page).to have_content "Rails"
      expect(page).not_to have_content "test rails"
      expect(page).not_to have_content "200.00"
    end

    it '商品情報から商品個別ページに移動できること、個別ページから元のページに戻れること' do
      click_on product1.name
      expect(current_path).to eq potepan_product_path(product1.id)
      expect(page).to have_content product1.name
      expect(page).to have_content product1.display_price
      expect(page).to have_content "一覧ページへ戻る"
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon1.id)
    end
  end
end
