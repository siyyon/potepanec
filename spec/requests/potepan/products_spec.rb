require 'rails_helper'

RSpec.describe "Spree::Product", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, taxons: [other_taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  describe "商品詳細ページ" do
    before { get potepan_product_path(product.id) }

    it "レスポンスが返ってくること" do
      expect(response).to be_successful
    end

    it "商品の詳細情報が表示されること" do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    context "関連商品がある場合" do
      it "関連商品が表示されていること" do
        related_products.each do |related_product|
          expect(response.body).to include related_product.display_price.to_s
        end
      end

      it "最大4件までしか表示されていないこと" do
        expect(response.body).not_to include related_products.last.name
      end

      it "関連していない商品が表示されていないこと" do
        expect(response.body).not_to include other_product.name
      end

      it "詳細表示されている商品は含まないこと" do
        expect(related_products).not_to include product
      end
    end
  end
end
