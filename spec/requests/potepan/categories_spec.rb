require 'rails_helper'

RSpec.describe "Spree::Taxon", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon1) { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: "T-Shirts", taxonomy: taxonomy) }
  let!(:product1) { create(:product, name: "test mug", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "test t-shirt", taxons: [taxon2]) }

  describe "商品一覧ページ" do
    before { get potepan_category_path(taxon1.id) }

    it "レスポンスが返ってくること" do
      expect(response).to be_successful
    end

    it "商品分類の名前が表示されること" do
      expect(response.body).to include taxonomy.name
      expect(response.body).to include taxon1.name
      expect(response.body).to include taxon2.name
    end

    it "見たい小分類の商品の名前、価格が表示されること" do
      expect(response.body).to include product1.name
      expect(response.body).to include product1.price.to_s
    end

    it "選んでいない小分類の商品が表示されてないこと" do
      expect(response.body).not_to include product2.name
    end
  end
end
