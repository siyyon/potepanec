require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title method" do
    context "page_titleが''の場合" do
      it "base_titleを表示する" do
        expect(full_title('')).to eq "BIGBAG Store"
      end
    end

    context "page_titleが''でない場合" do
      it "page_title - base_titleを表示する" do
        expect(full_title('example')).to eq "example - BIGBAG Store"
      end
    end
  end
end
