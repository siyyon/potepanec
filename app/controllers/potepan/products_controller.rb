class Potepan::ProductsController < ApplicationController
  MAX_NUM_OF_RERATED_PRODUCTS = 4
  def show
    @product = Spree::Product.find(params[:id])
    @taxons = @product.taxons
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).limit(MAX_NUM_OF_RERATED_PRODUCTS)
  end
end
